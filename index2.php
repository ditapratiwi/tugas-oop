<?php
$host   = "localhost";
$user   = "root";
$pass   = "";
$db     = "tokoh";

$koneksi    = mysqli_connect($host, $user, $pass, $db);
if (!$koneksi) {
    die("tidak bisa terkoneksi ke database");
} else {
    echo "Koneksi Berhasil";
}

$NAMA_PRODUK = "";
$MERK_PRODUK = "";
$JENIS_PRODUK = "";
$HARGA_PRODUK = "";
$sukses = "";
$error = "";

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}

if($op == 'delete'){
    $ID = $_GET ['ID'];
    $sql1 ="delete from mahasiswa where id = '$ID'";
    $q1 = mysqli_query ($koneksi, $sql1);

    if($q1){
        $sukses = "Berhasil Menghapus Data";
    }else{
        $error = "Gagal Menghapus Data";
    }

}

if ($op == 'edit') {
    $ID = $_GET['ID'];
    $sql1 = "select * from mahasiswa where id = '$ID'";
    $q1 = mysqli_query($koneksi, $sql1);
    $rq1 = mysqli_fetch_array($q1);
    $NAMA_PRODUK = $r1['NAMA_PRODUK'];
    $MERK_PRODUK = $r1['MERK_PRODUK'];
    $JENIS_PRODUK = $r1['JENIS_PRODUK'];
    $HARGA_PRODUK = $r1['HARGA_PRODUK'];

    if ($NIM == '') {
        $error = "Data Tidak Ditemukan";
    }
}

if (isset($_POST['Simpan'])) { //untuk create
    $NAMA_PRODUK = $_POST['NAMA_PRODUK'];
    $MERK_PRODUK = $_POST['MERK_PRODUK'];
    $JENIS_PRODUK = $_POST['JENIS_PRODUK'];
    $HARGA_PRODUK = $_POST['HARGA_PRODUK'];

    if ($NAMA_PRODUK && $MERK_PRODUK && $JENIS_PRODUK && $HARGA_PRODUK) {
        if ($op == 'edit') { //untuk update 
            $sql1 = "Update Mahasiswa Set nama = '$NAMA_PRODUK', merk= '$MERK_PRODUK', jenis ='$JENIS_PRODUK', harga = '$HARGA_PRODUK' where id = '$ID'";
            $q1 = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data Berhasil di Update";
            } else {
                $error = "Data Gagal di Update";
            }
        } else { //untuk insert
            $sql1 = "insert into mahasiswa (NAMA_PRODUK, MERK_PRODUK, JENIS_PRODUK, HARGA_PRODUK) values ('$NAMA_PRODUK', '$MERK_PRODUK', '$JENIS_PRODUK', '$HARGA_PRODUK')";
            $q1 = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Berhasil Memasukkan Data Baru";
            } else {
                $error = "Gagal Memasukkan Data";
            }
        }
    } else {
        $error = "Silahkan Masukkan Semua Data";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Produk</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }

      
    </style>

</head>

<body>
    <div class="mx-auto">
        <!-- untuk memasukkan data -->
        <div class="card">
            <div class="card-header" style= "background-color:grey;">
                Create / Edit Data
            </div>
            <div class="card-body">
                <?php
                if ($error) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php

                }
                ?>

                <?php
                if ($sukses) {
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $sukses ?>
                    </div>
                <?php

                }
                ?>
                <form action=" " method="POST" >
                <div class="from" style="background-color:aquamarine;">
                    <div class="mb-3">
                        <label for="NAMA_PRODUK" class="col-sm-2-col-form-label">NAMA_PRODUK</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="NAMA_PRODUK" name="NAMA_PRODUK" value="<?php echo $NAMA_PRODUK ?>">
                        </div>

                        <div class="mb-3">
                            <label for="MERK_PRODUK" class="col-sm-2-col-form-label">MERK_PRODUK</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="MERK_PRODUK" name="MERK_PRODUK" value="<?php echo $MERK_PRODUK ?>">
                            </div>

                            <div class="mb-3">
                                <label for="JENIS_PRODUK" class="col-sm-2-col-form-label">JENIS_PRODUK</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="JENIS_PRODUK" name="JENIS_PRODUK" value="<?php echo $JENIS_PRODUK ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="HARGA_PRODUK" class="col-sm-2-col-form-label">HARGA_PRODUK</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="HARGA_PRODUK" name="HARGA_PRODUK" value="<?php echo $HARGA_PRODUK ?>">
                                       
                                    </div>
                                    <div class="col-12">
                                        <input type="Submit" name="Simpan" value="Simpan Data" class="btn btn-primary">
                                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Produk
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">NAMA_PRODUK</th>
                            <th scope="col">MERK_PRODUK</th>
                            <th scope="col">JENIS_PRODUKT</th>
                            <th scope="col">HARGA_PRODUK</th>
                            <th scope="col">AKSI</th>
                        </tr>
                    <tbody>
                        <?php
                        $sql2 = "select * from produk order by ID desc";
                        $q2 = mysqli_query($koneksi, $sql2);
                        $urut = 1;
                        while ($r2= mysqli_fetch_array ($q2)){
                            $ID  = $r2['ID'];
                            $NAMA_PRODUK = $r2['NAMA_PRODUK'];
                            $MERK_PRODUK = $r2['MERK_PRODUK'];
                            $JENIS_PRODUK = $r2['JENIS_PRODUK'];
                            $HARGA_PRODUK = $r2['HARGA_PRODUK'];

                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $NAMA_PRODUK?> </td>
                                <td scope="row"><?php echo $MERK_PRODUK ?> </td>
                                <td scope="row"><?php echo $JENIS_PRODUK ?> </td>
                                <td scope="row"><?php echo $HARGA_PRODUK?> </td>
                                <td scope="row">
                                    <a href="index2.php?op = edit & id= <?php echo $ID ?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                    <a href = "index2.php?op = delete & id= <?php echo $ID ?> " onclick ="return confirm('Yakin Ingin Hapus Data?')"><button type="button" class="btn btn-danger">Delete</button></a> 
                                    

                                </td>
                            </tr>
                        <?php

                        }
                        ?>
                    </tbody>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</body>

</html>

