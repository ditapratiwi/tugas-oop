 <?php
$host   = "localhost";
$user   = "root";
$pass   = "";
$db     = "akademik";

$koneksi    = mysqli_connect($host, $user, $pass, $db);
if (!$koneksi) {
    die("tidak bisa terkoneksi ke database");
} else {
    echo "Koneksi Berhasil";
}

$NIM = "";
$NAMA = "";
$ALAMAT = "";
$PRODI = "";
$sukses = "";
$error = "";

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}

if($op == 'delete'){
    $ID = $_GET ['ID'];
    $sql1 ="delete from mahasiswa where id = '$ID'";
    $q1 = mysqli_query ($koneksi, $sql1);

    if($q1){
        $sukses = "Berhasil Menghapus Data";
    }else{
        $error = "Gagal Menghapus Data";
    }

}

if ($op == 'edit') {
    $ID = $_GET['ID'];
    $sql1 = "select * from mahasiswa where id = '$ID'";
    $q1 = mysqli_query($koneksi, $sql1);
    $rq1 = mysqli_fetch_array($q1);
    $NIM = $r1['NIM'];
    $NAMA = $r1['NAMA'];
    $ALAMAT = $r1['ALAMAT'];
    $PRODI = $r1['PRODI'];

    if ($NIM == '') {
        $error = "Data Tidak Ditemukan";
    }
}

if (isset($_POST['Simpan'])) { //untuk create
    $NIM = $_POST['NIM'];
    $NAMA = $_POST['NAMA'];
    $ALAMAT = $_POST['ALAMAT'];
    $PRODI = $_POST['PRODI'];

    if ($NIM && $NAMA && $ALAMAT && $PRODI) {
        if ($op == 'edit') { //untuk update 
            $sql1 = "Update Mahasiswa Set nim = '$NIM', nama= '$NAMA', alamat ='$ALAMAT', prodi = '$PRODI' where id = '$ID'";
            $q1 = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data Berhasil di Update";
            } else {
                $error = "Data Gagal di Update";
            }
        } else { //untuk insert
            $sql1 = "insert into mahasiswa (NIM, NAMA, ALAMAT, PRODI) values ('$NIM', '$NAMA', '$ALAMAT', '$PRODI')";
            $q1 = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Berhasil Memasukkan Data Baru";
            } else {
                $error = "Gagal Memasukkan Data";
            }
        }
    } else {
        $error = "Silahkan Masukkan Semua Data";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>

</head>

<body>
    <div class="mx-auto">
        <!-- untuk memasukkan data -->
        <div class="card">
            <div class="card-header">
                Create / Edit Data
            </div>
            <div class="card-body">
                <?php
                if ($error) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php

                }
                ?>

                <?php
                if ($sukses) {
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $sukses ?>
                    </div>
                <?php

                }
                ?>
                <form action=" " method="POST">
                    <div class="mb-3">
                        <label for="NIM" class="col-sm-2-col-form-label">NIM</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="NIM" name="NIM" value="<?php echo $NIM ?>">
                        </div>

                        <div class="mb-3">
                            <label for="NAMA" class="col-sm-2-col-form-label">NAMA</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="NAMA" name="NAMA" value="<?php echo $NAMA ?>">
                            </div>

                            <div class="mb-3">
                                <label for="ALAMAT" class="col-sm-2-col-form-label">ALAMAT</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="ALAMAT" name="ALAMAT" value="<?php echo $ALAMAT ?>">
                                </div>

                                <div class="mb-3">
                                    <label for="PRODI" class="col-sm-2-col-form-label">PRODI</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="PRODI" id="PRODI">
                                            <option value=""> - Pilih Prodi - </option>
                                            <option value="MIF" <?php if ($PRODI == "MIF") echo "Selected" ?>>MIF</option>
                                            <option value="TMM" <?php if ($PRODI == "TMM") echo "Selected" ?>>TMM</option>
                                            <option value="AKP" <?php if ($PRODI == "TMM") echo "Selected" ?>>AKP</option>
                                            <option value="MBP" <?php if ($PRODI == "TMM") echo "Selected" ?>>MBP</option>
                                        </select>
                                    </div>
                                    <div class="col-12">
                                        <input type="Submit" name="Simpan" value="Simpan Data" class="btn btn-primary">
                                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Mahasiswa
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">NIM</th>
                            <th scope="col">NAMA</th>
                            <th scope="col">ALAMAT</th>
                            <th scope="col">PRODI</th>
                            <th scope="col">AKSI</th>
                        </tr>
                    <tbody>
                        <?php
                        $sql2 = "select * from mahasiswa order by ID desc";
                        $q2 = mysqli_query($koneksi, $sql2);
                        $urut = 1;
                        while ($r2 = mysqli_fetch_array($q2)) {
                            $ID = $r2['ID'];
                            $NIM = $r2['NIM'];
                            $NAMA = $r2['NAMA'];
                            $ALAMAT = $r2['ALAMAT'];
                            $PRODI = $r2['PRODI'];

                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $NIM ?> </td>
                                <td scope="row"><?php echo $NAMA ?> </td>
                                <td scope="row"><?php echo $ALAMAT ?> </td>
                                <td scope="row"><?php echo $PRODI ?> </td>
                                <td scope="row">
                                    <a href="index1.php?op = edit & id= <?php echo $ID ?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                    <a href = "index1.php?op = delete & id= <?php echo $ID ?> " onclick ="return confirm('Yakin Ingin Hapus Data?')"><button type="button" class="btn btn-danger">Delete</button></a> 
                                    

                                </td>
                            </tr>
                        <?php

                        }
                        ?>
                    </tbody>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</body>

</html>