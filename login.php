<?php
$host   = "localhost";
$user   = "root";
$pass   = "";
$db     = "akademik";

$koneksi    = mysqli_connect($host, $user, $pass, $db);
if (!$koneksi) {
    die("tidak bisa terkoneksi ke database");
} else {
    echo "Koneksi Berhasil";
}

$name = "";
$email = "";
$telp = "";
$password= "";
$sukses = "";
$error = "";

if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}

if($op == 'delete'){
    $ID = $_GET ['ID'];
    $sql1 ="delete from login where id = '$id'";
    $q1 = mysqli_query ($koneksi, $sql1);

    if($q1){
        $sukses = "Berhasil Menghapus Data";
    }else{
        $error = "Gagal Menghapus Data";
    }

}

if ($op == 'edit') {
    $id = $_GET['ID'];
    $sql1 = "select * from login where id = '$id'";
    $q1 = mysqli_query($koneksi, $sql1);
    $rq1 = mysqli_fetch_array($q1);
    $name = $r1['name'];
    $email= $r1['email'];
    $telp = $r1['telp'];
    $password = $r1['password'];

    if ($name == '') {
        $error = "Data Tidak Ditemukan";
    }
}

if (isset($_POST['Simpan'])) { //untuk create
    $name = $_POST['name'];
    $email = $_POST['email'];
    $telp = $_POST['telp'];
    $password = $_POST['password'];

    if ($name && $email && $telp && $password) {
        if ($op == 'edit') { //untuk update 
            $sql1 = "Update login Set name = '$name', email= '$email', telp ='$telp', password = '$password' where id = '$id'";
            $q1 = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data Berhasil di Update";
            } else {
                $error = "Data Gagal di Update";
            }
        } else { //untuk insert
            $sql1 = "insert into login (name, email, telp, password) values ('$name', '$email', '$telp', '$password')";
            $q1 = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Berhasil Memasukkan Data Baru";
            } else {
                $error = "Gagal Memasukkan Data";
            }
        }
    } else {
        $error = "Silahkan Masukkan Semua Data";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>

</head>

<body>
    <div class="mx-auto">
        <!-- untuk memasukkan data -->
        <div class="card">
            <div class="card-header">
                Create / Edit Data
            </div>
            <div class="card-body">
                <?php
                if ($error) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php

                }
                ?>

                <?php
                if ($sukses) {
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $sukses ?>
                    </div>
                <?php

                }
                ?>
                <form action=" " method="POST">
                    <div class="mb-3">
                        <label for="name" class="col-sm-2-col-form-label">name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $name ?>">
                        </div>

                        <div class="mb-3">
                            <label for="email" class="col-sm-2-col-form-label">email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" name="email" value="<?php echo $email ?>">
                            </div>

                            <div class="mb-3">
                                    <label for="telp" class="col-sm-2-col-form-label">telp</label>
                                    <div class="col-sm-10">
                                    <input type="number" class="form-control" id="telp" name="telp" value="<?php echo $telp ?>">
                                    </div>

                            <div class="mb-3">
                                <label for="password" class="col-sm-2-col-form-label">password</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="password" name="password" value="<?php echo $password ?>">
                                </div>

                                
                                    <div class="col-12">
                                        <input type="Submit" name="Simpan" value="Simpan Data" class="btn btn-primary">
                                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Login User
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">name</th>
                            <th scope="col">email</th>
                            <th scope="col">telp</th>
                            <th scope="col">password</th>
                            <th scope="col">aksi</th>
                        </tr>
                    <tbody>
                        <?php
                        $sql2 = "select * from login order by ID desc";
                        $q2= mysqli_query($koneksi, $sql2);
                        $urut = 1;
                        while ($r=mysqli_fetch_array ($q2)){
                            $id = $r2['id'];
                            $name = $r2['name'];
                            $email = $r2['email'];
                            $telp = $r2['telp'];
                            $password = $r2['password'];

                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $name ?> </td>
                                <td scope="row"><?php echo $email ?> </td>
                                <td scope="row"><?php echo $telp ?> </td>
                                <td scope="row"><?php echo $password ?> </td>
                                <td scope="row">
                                    <a href="login.php?op = edit & id= <?php echo $id ?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                    <a href = "login.php?op = delete & id= <?php echo $id?> " onclick ="return confirm('Yakin Ingin Hapus Data?')"><button type="button" class="btn btn-danger">Delete</button></a> 
                                    

                                </td>
                            </tr>
                        <?php

                        }
                        ?>
                    </tbody>
                    </thead>
                </table>

            </div>
        </div>
    </div>
</body>

</html>